package AE_FHD_TestCase;

import AE_Pages.FHD.LoginPage;
import TestBase.BaseClass;
import com.aventstack.extentreports.ExtentReports;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import java.io.IOException;
public class LoginPageTest extends BaseClass {
    private static final Logger log= LogManager.getLogger(LoginPageTest.class);
    LoginPage loginPage;
    ExtentReports extents;

    @Test
    public void ApplicationLogin() throws IOException, ParseException {
       loginPage=new LoginPage(driver);
       loginPage.InvalidLogin(ReadJSONData("FhdUsername"), ReadJSONData("InvalidPwd"));
       log.info("User Unable to login due to Wrong Password...."+loginPage.errorText.getText());

       loginPage.ValidLogin(ReadJSONData("FhdUsername"),ReadJSONData("Password"));
       log.info("User ale to Login into an Application");

       loginPage.LogoutFromApplication();
       log.info("User able to logout from Application");
    }
}
