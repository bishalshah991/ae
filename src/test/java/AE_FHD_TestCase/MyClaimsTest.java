package AE_FHD_TestCase;

import AE_Pages.FHD.LoginPage;
import AE_Pages.FHD.MyClaims;
import TestBase.BaseClass;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class MyClaimsTest extends BaseClass {
    LoginPage loginPage;
    MyClaims myClaims;

    @Test
    public void TestClaimTabTest() throws IOException, ParseException, InterruptedException {
        loginPage=new LoginPage(driver);
        loginPage.ValidLogin(ReadJSONData("FhdUsername"),ReadJSONData("Password"));

        myClaims=new MyClaims(driver);
        myClaims.NavigateToMyClaims();
        myClaims.SearchPendingReviewPolicy(ReadJSONData("PolicySearch"));

        String Act_output=myClaims.VerifyStatus();
        Assert.assertEquals(Act_output,"Pending Review","String is not Matched");
        myClaims.ClickAction();
        myClaims.NavigateToAdditionalInfo();
        myClaims.navigatetoviewchecklistab();
        myClaims.CustomerSupportTab();
        myClaims.EnterBeneficiaryDetail();
        loginPage.LogoutFromApplication();
    }
}
