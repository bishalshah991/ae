package AE_FHD_TestCase;

import AE_Pages.FHD.LoginPage;
import AE_Pages.FHD.MyProfile;
import TestBase.BaseClass;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import java.io.IOException;

public class MyProfileTest extends BaseClass {
    LoginPage loginPage;
    MyProfile myProfile;

    @Test
    public void MyProfileTest() throws IOException, ParseException, InterruptedException {
        loginPage=new LoginPage(driver);
        loginPage.ValidLogin(ReadJSONData("FhdUsername"),ReadJSONData("Password"));

        myProfile=new MyProfile(driver);
        myProfile.ClickMyProfileIcon();
        myProfile.NavigateToMyProfileTab();
        myProfile.EnterMyProfileInfo(ReadJSONData("FirstName"),ReadJSONData("LastName"),ReadJSONData("TitleInput"));
        loginPage.LogoutFromApplication();
    }

}
