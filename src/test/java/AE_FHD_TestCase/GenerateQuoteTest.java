package AE_FHD_TestCase;

import AE_Pages.FHD.GenerateQuote;
import AE_Pages.FHD.LoginPage;
import TestBase.BaseClass;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

public class GenerateQuoteTest extends BaseClass {



    @Test
    public void TestGenerateQuote() throws IOException, ParseException, AWTException, InterruptedException {
        LoginPage loginPage=new LoginPage(driver);
        loginPage.ValidLogin(ReadJSONData("FhdUsername"),ReadJSONData("Password"));

        GenerateQuote generateQuote=new GenerateQuote(driver);
        generateQuote.SearchPolicy(ReadJSONData("PolicySearch"));
        generateQuote.EnterDateOfDeath();
        generateQuote.EnterCauseOfDeath();
        generateQuote.PickAwayFromHomeBenefit();
        generateQuote.UploadDocument();
        generateQuote.EnterSignature();
        generateQuote.ClaimInformation();

        String ActOutput=generateQuote.CheckTheSatausPendingReview();
        Assert.assertEquals(ActOutput,"Pending Review","String is not Matched");
        generateQuote.ClickAdditionalInfo();
        generateQuote.ClickCustomerSupport();
        loginPage.LogoutFromApplication();
    }
}
