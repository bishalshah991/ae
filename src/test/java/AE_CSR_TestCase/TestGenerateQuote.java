package AE_CSR_TestCase;

import AE_Pages.CSR.CsrPolicyDetail;
import AE_Pages.FHD.LoginPage;
import TestBase.BaseClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestGenerateQuote extends BaseClass {
    private static final Logger log= LogManager.getLogger(TestGenerateQuote.class);

    @Test(testName = "Valid")
    public void GenerateQuoteTest() throws IOException, ParseException, InterruptedException {
        LoginPage loginPage=new LoginPage(driver);

        loginPage.ValidCSRLogin(ReadJSONData("CsrUsername"),ReadJSONData("CsrPassword"));
        extentTest.info("User Logged into an Application");


    }


}
