package TestBase;

import AE_Pages.FHD.LoginPage;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Locale;

public class BaseClass {
    static String path="/Users/mac/Documents/Backup/Document/Automation/ExtentReport/AllTest.html";
    private static final Logger log= LogManager.getLogger(BaseClass.class);
    String n;
    public static WebDriver driver;
    LoginPage loginPage;
    public static ExtentReports extentReports;
    public static ExtentTest extentTest;

    @Parameters("browserName")
    @BeforeTest
    public void Setup(ITestContext context,@Optional("chrome") String browserName) throws IOException, ParseException {
        LaunchBrowser(browserName);
        Capabilities capabilities=((RemoteWebDriver)driver).getCapabilities();
        String device = capabilities.getBrowserName() + "" + capabilities.getBrowserVersion().substring(0, capabilities.getBrowserVersion().indexOf("."));
        String author = context.getCurrentXmlTest().getParameter("author");


        extentTest=extentReports.createTest(context.getName());
        extentTest.assignAuthor(author);
        extentTest.assignDevice(device);
    }

    public String ReadJSONData(String keyword) throws IOException, org.json.simple.parser.ParseException {

        Reader reader = new FileReader("src/main/resources/TestData/Ae_Data.json");
        JSONParser parser = new JSONParser();
        JSONArray jsonArr = (JSONArray) parser.parse(reader);
        for (Object obj : jsonArr) {

            JSONObject jo = (JSONObject) obj;
            n = (String) jo.get(keyword);
        }
        return n;
    }

    public void LaunchBrowser(String browser) throws IOException, ParseException {

        String ee=System.getProperty("os.name");

        log.info("Applcation Run On...."+ee);

        switch (browser.toLowerCase(Locale.ROOT))
        {

            case "chrome":
                WebDriverManager.chromedriver().setup();
                driver=new ChromeDriver();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver=new FirefoxDriver();
                break;
            case "safari":
                driver=new SafariDriver();
                break;

            default:
                driver=null;
                break;
        }

        driver.manage().window().maximize();
        driver.get(ReadJSONData("Website"));
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofMinutes(4));
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'LOGIN TO YOUR ACCOUNT')]")));
        log.info("Application is fully Loaded");
    }

    public static String captureScreenshot1(String filename)
    {
        TakesScreenshot takesScreenshot=(TakesScreenshot) driver;
        File sourcefile=takesScreenshot.getScreenshotAs(OutputType.FILE);
        File destFile=new File("./Screenshots/"+filename);

        try {
            FileUtils.copyFile(sourcefile,destFile);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        System.out.println("Screenshot saves successfully");
        return destFile.getAbsolutePath();
    }

    @AfterTest
    public void Quit() throws IOException, InterruptedException {

        driver.quit();
    }

    @AfterMethod
    public void checkStatus(Method m,ITestResult result)
    {
        if(result.getStatus()==ITestResult.FAILURE)
        {
            String screenshotPath=null;
            screenshotPath=captureScreenshot1(result.getTestContext().getName()+"_"+result.getMethod().getMethodName()+".jpg");
            extentTest.addScreenCaptureFromPath(screenshotPath);
            extentTest.fail(result.getThrowable());
        } else if (result.getStatus()==ITestResult.SUCCESS) {
            extentTest.pass(m.getName() + "is Passed");
        }
        extentTest.assignCategory(m.getAnnotation(Test.class).groups());

    }

    @BeforeSuite
    public void initialiseExtentReports()
    {
        ExtentSparkReporter sparkReporter_all=new ExtentSparkReporter(path);
        extentReports=new ExtentReports();
        extentReports.attachReporter(sparkReporter_all );

        extentReports.setSystemInfo("OS",System.getProperty("os.name"));
        extentReports.setSystemInfo("Java Version",System.getProperty("java.version"));
    }

    @AfterSuite
    public void generateReport() throws IOException {
        extentReports.flush();
        Desktop.getDesktop().browse(new File(path).toURI());
    }


}
