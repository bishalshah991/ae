package AE_Pages.CSR;
import TestBase.BaseClass;
import Utility.Locators;
import Utility.PolicySearch;
import helper.*;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class CsrPolicyDetail {
    String path="/Users/mac/Documents/Backup/Document/Automation/src/main/resources/TestData/TestPDFData.pdf";
    WebDriver driver;
    WaitHelper waitHelper;
    AddBeneficiaries addBeneficiaries;
    Locators locators;
    DropDownHelper dropDownHelper;
    PolicySearch policySearchHelper;
    BaseClass baseClass;

    @FindBy(xpath = "//span[contains(text(),'CANCEL')]")
    WebElement CancelButton;

    @FindBy(xpath = "//mat-datepicker-toggle[@class='mat-datepicker-toggle']/button/span[1]")
    WebElement CalenderIcon;

    @FindBy(xpath = "//div[contains(@class,'mat-calendar-arrow')]")
    WebElement PickYear;

    /* Drop Down of Cause of death */
    @FindBy(xpath = "//mat-select[@id='causeofdeath']")
    WebElement ClickCauseOfDeath;


    @FindBy(xpath = "//div[@class='cdk-overlay-pane']/div/div/mat-option[3]/span[contains(text(),'Accidental')]")
    WebElement TextofAcdidential;

    /*
        Radio Button
     */
    @FindBy(xpath = "//mat-label[contains(text(),'Entire Benefit')]")
    WebElement EntireBenefitRadio;

    @FindBy(xpath = "//form[starts-with(@class,'ng-tns')]/div[3]/fuse-input-radio-control/div[1]/mat-radio-group/mat-radio-button[1]/label/span[2]")
    WebElement AwayFromBenefit;

    /*
    Gnerate QUote Button
     */
    @FindBy(xpath = "//button[@id='generate-quote']")
    WebElement GenerateQuoteButton;

    /*
    Start Claim Button
     */
    @FindBy(xpath = "//span[contains(text(),'START CLAIM')]")
    WebElement StartClaimButton;

    /*
    Claim Type
     */
    @FindBy(xpath = "//mat-select[@id='claim_type']")
    WebElement ClickClaimType;

    @FindBy(xpath = "//div[@id='claim_type-panel']/mat-option[4]/span[contains(text(),'Phone')]")
    WebElement TextPhone;

    /*
    Relationship
     */
    @FindBy(xpath = "//mat-select[@id='relationship']")
    WebElement ClickRelationship;

    @FindBy(xpath = "//div[@id='relationship-panel']/mat-option[2]/span[contains(text(),'Beneficiary')]")
    WebElement TextBeneficiary;

    @FindBy(xpath = "//input[@id='phone_fax']")
    WebElement TextPhoneFax;

    @FindBy(xpath = "//mat-checkbox[starts-with(@id,'mat-checkbox')]/label/span[1]")
    WebElement ClickCheckBox;

    @FindBy(xpath = "//span[contains(text(),'ACCEPT')]")
    WebElement ClickAcceptButton;

    @FindBy(xpath = "//span[contains(text(),'SUBMIT CLAIM')]")
    WebElement ClcikSubmitClaim;

    @FindBy(xpath = "//span[contains(text(),'Death Claim Form')]")
    WebElement ContentDeathClaimForm;

    @FindBy(xpath = "//div[contains(text(),'BENEFICIARIES')]")
    WebElement ContentBeneficiary;

    /*
    Upload Document
     */
    @FindBy(xpath = "//input[@type='file']")
    public WebElement UploadDocument;


    @FindBy(xpath = "//div[@id='caseload']/div[1]/button[1]/span[1][contains(text(),'Save')]")
    WebElement SaveButton;

    /*
    Navigate to Match This
     */
    @FindBy(xpath = "//div[@ fxlayoutalign ='center']/div[1]/div/div[3]/button/span[1]/span")
    WebElement ClickFirstMatchThis;

    @FindBy(xpath = "//div[@ fxlayoutalign ='center']/div[2]/div/div[3]/button/span[1]/span")
    WebElement ClickSecondMatchthis;

    @FindBy(xpath = "//div[@fxlayoutalign='center']/div[1]/div/div[1]/div[2]/div")
    WebElement CompletedOne;

    @FindBy(xpath = "//div[@fxlayoutalign='center']/div[2]/div/div[1]/div[2]/div")
    WebElement CompletedTwo;

    @FindBy(xpath = "//div[@fxlayoutalign='center']/div[3]/div/div[1]/div[2]/div")
    WebElement Uploaded;


    /*
    Assign to me
     */
    @FindBy(xpath = "//div[contains(text(),'ASSIGN TO ME')]")
    public WebElement AssignMeText;

    @FindBy(xpath = "//h2[contains(text(),'Please confirm if you would like to assign this claim to yourself')]")
    WebElement AssignMeConfirmDialog;

    @FindBy(xpath = "//span[contains(text(),'CONFIRM')]")
    WebElement ConfirmButton;

    @FindBy(xpath = "//div[contains(text(),'APPROVE')]")
    WebElement ApproveTab;

    @FindBy(xpath = "//div[contains(text(),'NEW REQUIREMENT DOC')]")
    WebElement NewRequirement;

    /*
    Payment
     */

    @FindBy(xpath = "//div[@id='caseload']/fuse-action-panel/div/div/div[2]/button[4]/span[1]/mat-icon")
    WebElement PaymentTab;

    /*
    Mark Settled
     */
    @FindBy(xpath = "//div[contains(text(),'MARK SETTLED')]")
    WebElement MarkSettled;

    @FindBy(xpath = "//div[contains(text(),'TERMINATE & PAY')]")
    WebElement TerminatePay;

    @FindBy(xpath = "//div[contains(text(),'EXPORT CLAIM PACKET')]")
    WebElement ExportClaimPacket;

    @FindBy(xpath = "//span[contains(text(),'Settled')]")
    WebElement Settled;
    @FindBy(xpath = "//div[contains(text(),'Settled')]")
    WebElement ListStatusSettled;
    @FindBy(xpath = "//span[contains(text(),'Product Detail')]")
    WebElement ProductDetail;



    public CsrPolicyDetail(WebDriver driver)
    {
        this.driver=driver;
        policySearchHelper=new PolicySearch(driver);
        locators=new Locators(driver);
        dropDownHelper=new DropDownHelper(driver);
        waitHelper=new WaitHelper(driver);
        baseClass=new BaseClass();
        addBeneficiaries=new AddBeneficiaries(driver);
        PageFactory.initElements(driver,this);
    }

    public void SearchPolicy() throws IOException, ParseException {
        policySearchHelper.PolicySearchTab();
        policySearchHelper.EnterPolicyNumber();
        policySearchHelper.GotoAction();
        policySearchHelper.ClickBeginClaim();
    }

    public void ClickAssignFH() throws InterruptedException {
        locators.ClickAssignFh.click();
        Thread.sleep(2000);
        locators.CrossIcon.click();
    }

    public void ClickGeneratequote() throws InterruptedException {
        Thread.sleep(3000);
        locators.GenerateQuote.click();
        Thread.sleep(3000);
    }

    public void EnterdateofDeath()
    {
        String year="2022";
        String month="OCT";
        String day="10";

        CalenderIcon.click();
        PickYear.click();
        driver.findElement(By.xpath("//div[contains(text(),'"+year+"')]")).click();
        driver.findElement(By.xpath("//div[contains(text(),'"+month+"')]")).click();
        driver.findElement(By.xpath("//table[@class='mat-calendar-table']/tbody/tr/td/div[contains(text(),'"+day+"')]")).click();
    }

    public void EnterCauseofDeath() throws InterruptedException {
        ClickCauseOfDeath.click();
        String locatorXpath="//div[@class='cdk-overlay-pane']/div/div/mat-option";
        String value=TextofAcdidential.getText();
        System.out.println(value);
        dropDownHelper.BootStrapDropDown(locatorXpath,value);
    }
    public void ClickEntireBenefit()
    {
        AwayFromBenefit.click();
        EntireBenefitRadio.click();
        GenerateQuoteButton.click();
        waitHelper.waitForElementPresent(driver,StartClaimButton,40);
    }

    public void Quotepage() throws InterruptedException {
        StartClaimButton.click();
        Thread.sleep(2000);
    }

    public void ClaimTypeDropDown()
    {
        ClickClaimType.click();
        String xpath_value="//div[@id='claim_type-panel']/mat-option";
        String Phone_value=TextPhone.getText();
        dropDownHelper.BootStrapDropDown(xpath_value,Phone_value);
    }

    public void RelationshipDropDown() throws IOException, ParseException {
        ClickRelationship.click();
        String xpath_value="//div[@id='relationship-panel']/mat-option";
        String Text_Beneficiary=TextBeneficiary.getText();
        dropDownHelper.BootStrapDropDown(xpath_value,Text_Beneficiary);
        TextPhoneFax.sendKeys(baseClass.ReadJSONData("Fax"));
        ClickCheckBox.click();
        ClickAcceptButton.click();
        waitHelper.waitForElementPresent(driver,ClcikSubmitClaim,20);
    }

    public void ClaimInformation()
    {
        ClcikSubmitClaim.click();
        waitHelper.waitForElementPresent(driver,ContentDeathClaimForm,20);
        waitHelper.waitForElementPresent(driver,ContentBeneficiary,20);
    }

    public void EnterBeneficiary() throws InterruptedException, IOException, ParseException {
        addBeneficiaries.AddBeneficiary();
        addBeneficiaries.CreateBeneficiary();
        addBeneficiaries.EnterFnameLname();
        addBeneficiaries.EnterDomesTicAddress();
    }

    public void UploadDocument() throws InterruptedException {
        UploadDocument.sendKeys(path);
        waitHelper.waitForElementPresent(driver,SaveButton,10);
    }

    public void ClickMatchThis() throws InterruptedException {
        ClickFirstMatchThis.click();
        ClickSecondMatchthis.click();
        Thread.sleep(2000);
        SaveButton.click();
    }

    public void CheckstatusCompleted() throws InterruptedException {
        waitHelper.waitForElementPresent(driver,CompletedOne,40);
        waitHelper.waitForElementPresent(driver,CompletedTwo,40);
        waitHelper.waitForElementPresent(driver,Uploaded,40);
        Thread.sleep(20000);
        driver.navigate().refresh();
        waitHelper.waitForElementPresent(driver,CompletedOne,40);
        waitHelper.waitForElementPresent(driver,AssignMeText,40);
    }

    public void ClickAssignMe() throws InterruptedException {
        AssignMeText.click();
        Thread.sleep(2000);
        ConfirmButton.click();
        Thread.sleep(20000);
        driver.navigate().refresh();
        waitHelper.waitForElementPresent(driver,CompletedOne,40);
        waitHelper.waitForElementPresent(driver,NewRequirement,40);
        waitHelper.waitForElementPresent(driver,ApproveTab,40);
    }

    public void ClickApproveButton() throws InterruptedException {
        ApproveTab.click();
        Thread.sleep(2000);
        ConfirmButton.click();
        waitHelper.waitForElementPresent(driver,ApproveTab,40);
    }

    public void ClickPayment() throws InterruptedException {
        Thread.sleep(3000);
        PaymentTab.click();
        waitHelper.waitForElementPresent(driver,ApproveTab,40);
        waitHelper.waitForElementPresent(driver,MarkSettled,20);
        ApproveTab.click();
        waitHelper.waitForElementPresent(driver,ApproveTab,20);
        Thread.sleep(2000);
        ConfirmButton.click();
        waitHelper.waitForElementPresent(driver,TerminatePay,20);
    }

    public void TeminateAndPay() throws InterruptedException {
        TerminatePay.click();
        Thread.sleep(2000);
        ConfirmButton.click();
        Thread.sleep(2000);
        waitHelper.waitForElementPresent(driver,PaymentTab,20);
        waitHelper.waitForElementPresent(driver,ProductDetail,20);

    }

    public void NavigateToPolicySearchTab() throws IOException, ParseException {
        waitHelper.waitForElementPresent(driver,Settled,20);
        policySearchHelper.PolicySearchTab();
        policySearchHelper.EnterPolicyNumber();
        waitHelper.waitForElementPresent(driver,ListStatusSettled,20);
    }





}
