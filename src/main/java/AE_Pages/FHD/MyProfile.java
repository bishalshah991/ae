package AE_Pages.FHD;


import helper.WaitHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyProfile {
    WebDriver driver;
    WaitHelper waitHelper;

    @FindBy(xpath = "//mat-icon[contains(text(),'keyboard_arrow_down')]")
    public WebElement MyProfile;
    @FindBy(xpath = "//div[starts-with(@class,'mat-menu-panel')]")
    WebElement Menu;

    @FindBy(xpath = "//div[starts-with(@class,'mat-menu-panel')]/div/button[1]/span[contains(text(),'My Profile')]")
    WebElement MyProfileTab;

    @FindBy(xpath = "//input[@data-placeholder='First Name']")
    WebElement FirstName;

    @FindBy(xpath = "//input[@data-placeholder='Last Name']")
    WebElement LastName;

    @FindBy(xpath = "//input[@data-placeholder='Title']")
    WebElement Title;
    @FindBy(xpath = "//mat-icon[contains(text(),'check_circle')]")
    WebElement Save;

    @FindBy(xpath = "//mat-dialog-container[starts-with(@id,'mat-dialog')]/fuse-app-error-dialog/mat-dialog-actions/button/span[1][contains(text(),'CLOSE')]")
    WebElement Diolog;

    @FindBy(xpath = "//span[contains(text(),'CLOSE')]")
    WebElement CloseButton;




    public MyProfile(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
        PageFactory.initElements(driver,this);
    }

    public void ClickMyProfileIcon(){
        MyProfile.click();
        waitHelper.waitForElementPresent(driver,Menu,10);
    }

    public void NavigateToMyProfileTab() throws InterruptedException {
        MyProfileTab.click();
        Thread.sleep(2000);
    }

    public void EnterMyProfileInfo(String fname,String lname,String title) throws InterruptedException {
        FirstName.clear();
        FirstName.sendKeys(fname);

        LastName.clear();
        LastName.sendKeys(lname);

        Title.clear();
        Title.sendKeys(title);
        Save.click();

        waitHelper.waitForElementPresent(driver,Diolog,10);
        CloseButton.click();
        Thread.sleep(2000);

        Save.click();
        waitHelper.waitForElementPresent(driver,CloseButton,10);
        CloseButton.click();
        Thread.sleep(2000);
    }





}
