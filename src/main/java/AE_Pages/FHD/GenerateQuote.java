package AE_Pages.FHD;

import helper.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.net.URISyntaxException;

public class GenerateQuote {
    WebDriver driver;
    WaitHelper waitHelper;
    DropDownHelper dropDownHelper;
    AdditionalInfo additionalInfo;
    CustomerSupport customerSupport;
    ViewCheckList viewCheckList;

    String path="/Users/mac/Documents/Backup/Document/Automation/src/main/resources/Picture.png";

    @FindBy(xpath = "//input[@id='searchInput']")
    WebElement PolicySearch;
    @FindBy(xpath = "//button[contains(text(),'Start')]")
    WebElement Start;
    @FindBy(xpath = "//mat-row[@role='row']/mat-cell[5]/div/button/span[1]")
    WebElement Action;
    @FindBy(xpath = "//div[starts-with(@class,'mat-menu-content')]")
    WebElement ActionPopup;

    @FindBy(xpath = "//div[starts-with(@class,'mat-menu-content')]/button[1]/span")
    WebElement BeginClaim;

    @FindBy(xpath = "//span[contains(text(),'NO')] ")
    WebElement ClickNoButton;

    @FindBy(xpath = " //span[contains(text(),'GENERATE QUOTE')]")
    WebElement GenerateQuoteButton;

    /*
        Date of Death
     */

    @FindBy(xpath = "//div[starts-with(@class,'content ng-tns')]/form/div[1]/fuse-input-date-control/mat-datepicker-toggle/button/span[1]")
    WebElement DateofDeath;

    @FindBy(xpath = "//div[contains(@class,'mat-calendar-arrow')]")
    WebElement PickYear;
    /*
    Cause of Death
     */


    @FindBy(xpath = "//div[starts-with(@id,'mat-select-value')]")
    WebElement CauseOfDeath;

    @FindBy(xpath = "//div[@role='listbox']")
    WebElement ListOfBox;

    @FindBy(xpath = "//div[@role='listbox']/mat-option")
    WebElement Option;

    @FindBy(xpath = "//div[@role='listbox']/mat-option[3]")
    WebElement Accidental;

    /*
    Away from Benefit
     */
    @FindBy(xpath = "//form[starts-with(@class,'ng-tns')]/div[3]/fuse-input-radio-control/div[1]/mat-radio-group/mat-radio-button[1]/label/span[2]")
    WebElement AwayFromBenefit;

    /*
    Enter Benefit
     */

    @FindBy(xpath = "//mat-label[contains(text(),'Entire Benefit')]")
    WebElement EntireBenefit;

    /*
    Start Claim
     */

    @FindBy(xpath = "//span[contains(text(),'START CLAIM')]")
    WebElement StartClaim;

    /*
    Claim Information
     */

    @FindBy(xpath = "//button[@id='claimantSignature']")
    WebElement SignButton;

    /*
    Click Here to Upload
     */
    @FindBy(xpath = "//div[starts-with(@class,'bubble doc asking flex-col')]/button/span[1]")
    WebElement UploadButton;

    /*
    File Upload
     */
    @FindBy(xpath = "//input[@type='file']")
    public WebElement UploadDocument;

    /*
    Wait for the Image
     */
    @FindBy(xpath = "//div[@class='ng-star-inserted']/button/span[1]/img")
    WebElement WaitForImage;

    /*Signauter
     */

    @FindBy(xpath = "//button[@id='claimantSignature']")
    WebElement Signautre;

    @FindBy(xpath = "//span[contains(text(),'Signature Pad Dialog')]")
    WebElement ToolDiolog;

    @FindBy(xpath = "//mat-checkbox[starts-with(@id,'mat-checkbox')]/label/span[1]")
    WebElement CheckBox;

    @FindBy(xpath = "//span[contains(text(),'ACCEPT')]")
    WebElement AcceptButton;

    @FindBy(xpath = "//div[@class='canvasWrapper']")
    WebElement ClaimForm;

    @FindBy(xpath = "//span[contains(text(),'SUBMIT CLAIM')]")
    WebElement SubmitClaimButton;

    @FindBy(xpath = "//div[contains(text(),'Thank You.')]")
    WebElement ThankYouPage;

    /*
    Check the status Pending Review
     */
    @FindBy(xpath = "//div[starts-with(@class,'header1')]/div[1]/div[2]/span[contains(text(),'Pending Review')]")
    WebElement PendingReview;


    public GenerateQuote(WebDriver driver) {
        this.driver = driver;
        waitHelper = new WaitHelper(driver);
        additionalInfo=new AdditionalInfo(driver);
        dropDownHelper = new DropDownHelper(driver);
        customerSupport=new CustomerSupport(driver);
        viewCheckList=new ViewCheckList(driver);
        PageFactory.initElements(driver, this);
    }

    public void SearchPolicy(String policyNumber)
    {
        PolicySearch.sendKeys(policyNumber);
        waitHelper.waitForElementPresent(driver,Start,20);

        Action.click();
        waitHelper.waitForElementPresent(driver,ActionPopup,10);

        BeginClaim.click();
        waitHelper.waitForElementPresent(driver,GenerateQuoteButton,20);

//        ClickNoButton.click();
//        waitHelper.waitForElementPresent(driver,GenerateQuoteButton,20);
    }

    public void EnterDateOfDeath()
    {
        String year="2022";
        String month="OCT";
        String day="10";

        DateofDeath.click();
        PickYear.click();
        driver.findElement(By.xpath("//div[contains(text(),'"+year+"')]")).click();
        driver.findElement(By.xpath("//div[contains(text(),'"+month+"')]")).click();
        driver.findElement(By.xpath("//table[@class='mat-calendar-table']/tbody/tr/td/div[contains(text(),'"+day+"')]")).click();

    }

    public void EnterCauseOfDeath()
    {
        String xpaht="//div[@role='listbox']/mat-option";
        CauseOfDeath.click();
        waitHelper.waitForElementPresent(driver,ListOfBox,10);
        dropDownHelper.BootStrapDropDown(xpaht,Accidental.getText());
    }

    public void PickAwayFromHomeBenefit()
    {
        AwayFromBenefit.click();
        EntireBenefit.click();
        GenerateQuoteButton.click();
        waitHelper.waitForElementPresent(driver,StartClaim,20);
        StartClaim.click();
        waitHelper.waitForElementPresent(driver,SignButton,20);
    }

    public void UploadDocument() throws InterruptedException {
        UploadDocument.sendKeys(path);
        waitHelper.waitForElementPresent(driver,WaitForImage,10);
    }

    public void EnterSignature() throws InterruptedException {
       SignatureHelper signatureHelper=new SignatureHelper(driver);
       signatureHelper.SignatureHelperClass();
       CheckBox.click();
       AcceptButton.click();
    }

    public void ClaimInformation()
    {
        waitHelper.waitForElementPresent(driver,ClaimForm,20);
        SubmitClaimButton.click();
        waitHelper.waitForElementPresent(driver,ThankYouPage,20);
    }

    public String CheckTheSatausPendingReview()
    {
        return PendingReview.getText();
    }

    public void ClickAdditionalInfo() throws InterruptedException {
        additionalInfo.ClickAdditionalInfoTab();
    }

    public void ClickCustomerSupport() throws InterruptedException {
        customerSupport.GotoCustomerSupport();
        customerSupport.TypeYourMessage();
    }

    public void ViewChecklist()
    {
        viewCheckList.ClickviewChecklist();
        viewCheckList.ClickBackArrow();
    }




}
