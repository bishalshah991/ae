package AE_Pages.FHD;

import TestBase.BaseClass;
import helper.*;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.security.Key;

public class MyClaims {
    WebDriver driver;
    WaitHelper waitHelper;
    BaseClass baseClass;
    ViewCheckList viewCheckList;
    CustomerSupport customerSupport;

    AdditionalInfo additionalInfo;
    AddBeneficiaries addBeneficiaries;

    @FindBy(xpath = "//span[contains(text(),'My Claims')]")
    WebElement MyClaimtab;

    @FindBy(xpath = "//span[contains(text(),'Policy Number')]")
    WebElement MyClaimPageLoad;

    @FindBy(xpath = "//input[@placeholder='Search within the following claims...']")
    WebElement SearchTextBox;

    @FindBy(xpath = "//strong[contains(text(),'Pending Review')]")
    WebElement PendingReviewStatus;

    @FindBy(xpath = "//mat-row[@role='row']/mat-cell[6]/div/button/span[1]/mat-icon ")
    WebElement Action;

    @FindBy(xpath = "//span[contains(text(),'View Claims Details')]")
    WebElement ViewClaimDetails;

    @FindBy(xpath = "//span[contains(text(),'Claim Detail')]")
    WebElement ClaimDetails;


    public MyClaims(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
        addBeneficiaries=new AddBeneficiaries(driver);
        baseClass=new BaseClass();
        additionalInfo=new AdditionalInfo(driver);
        viewCheckList=new ViewCheckList(driver);
        customerSupport=new CustomerSupport(driver);
        PageFactory.initElements(driver,this);
    }

    public void NavigateToMyClaims()
    {
        MyClaimtab.click();
        waitHelper.waitForElementPresent(driver,MyClaimPageLoad,20);
    }

    public void SearchPendingReviewPolicy(String policyNum)
    {
        SearchTextBox.sendKeys(policyNum);
        SearchTextBox.sendKeys(Keys.ENTER);
        waitHelper.waitForElementPresent(driver,PendingReviewStatus,20);
    }

    public String VerifyStatus()
    {
        return PendingReviewStatus.getText();
    }

    public void ClickAction()
    {
        Action.click();
        waitHelper.waitForElementPresent(driver,ViewClaimDetails,20);
        ViewClaimDetails.click();
        waitHelper.waitForElementPresent(driver,ClaimDetails,20);
    }

    public void EnterBeneficiaryDetail() throws IOException, ParseException, InterruptedException {
        addBeneficiaries.AddBeneficiary();
        addBeneficiaries.CreateBeneficiary();
        addBeneficiaries.EnterFnameLname();
        addBeneficiaries.EnterDomesTicAddress();
    }

    public void NavigateToAdditionalInfo() throws InterruptedException {
        additionalInfo.ClickAdditionalInfoTab();
    }

    public void navigatetoviewchecklistab()
    {
        viewCheckList.ClickviewChecklist();
        viewCheckList.ClickBackArrow();
    }

    public void CustomerSupportTab() throws InterruptedException {
        customerSupport.GotoCustomerSupport();
        customerSupport.TypeYourMessage();
    }

}
