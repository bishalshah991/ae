package AE_Pages.FHD;


import helper.WaitHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    WebDriver driver;
    WaitHelper waitHelper;
    @FindBy(xpath = "//input[@formcontrolname='email']")
    public WebElement username;

    @FindBy(xpath = "//input[@id='mat-input-1']")
    WebElement password;

    @FindBy(xpath = "//span[contains(text(),'LOGIN')]")
    WebElement loginButton;


    //alert when the username and password is incorrect
    @FindBy(xpath = "//mat-error[contains(text(),'Username or Password is Incorrect')]")
    public WebElement errorText;

    @FindBy(xpath = "//h1[contains(text(),'LOGIN TO YOUR ACCOUNT')]")
    WebElement textLogin;




    /*
        Wait for the Search Box
     */

    @FindBy(xpath = "//input[@id='searchInput']")
    WebElement SearchTextBox;

    @FindBy(xpath = "//div[contains(text(),'Payment Overview')]")
    WebElement CSRDashboard;

    /*
        Logout from the appliction
     */

    @FindBy(xpath = "//mat-icon[contains(text(),'keyboard_arrow_down')]")
    public WebElement Logout;

    @FindBy(xpath = "//span[contains(text(),'Sign Out')]")
    WebElement SignoutButton;



    public LoginPage(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
        waitHelper=new WaitHelper(driver);
    }

    public void InvalidLogin(String uname, String passWord)
    {
        username.clear();
        username.sendKeys(uname);

        password.clear();
        password.sendKeys(passWord);
        loginButton.click();
        waitHelper.waitForElementPresent(driver,errorText,10);
    }

    public String GetAlert()
    {
        return errorText.getText();
    }

    public void ValidLogin(String uname, String passWord)
    {
        username.clear();
        username.sendKeys(uname);

        password.clear();
        password.sendKeys(passWord);
        loginButton.click();
        waitHelper.waitForElementPresent(driver,SearchTextBox,10);
    }

        public void ValidCSRLogin(String uname, String passWord)
    {
        username.clear();
        username.sendKeys(uname);

        password.clear();
        password.sendKeys(passWord);
        loginButton.click();
        waitHelper.waitForElementPresent(driver,CSRDashboard,10);
    }

    public void LogoutFromApplication()
    {
        Logout.click();
        waitHelper.waitForElementPresent(driver,SignoutButton,10);
        SignoutButton.click();
    }

   }
