package Utility;

import TestBase.BaseClass;
import helper.WaitHelper;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class PolicySearch {

    WebDriver driver;
    WaitHelper waitHelper;
    BaseClass baseClass;

    @FindBy(xpath = "//div[@class='navbar-content']/fuse-navigation/div/fuse-nav-vertical-item[3]/div[@class='Policy Search']")
    WebElement PolicySearchTab;

    @FindBy(xpath = "//input[@id='searchInput']")
    WebElement PolicySearchTextBox;

    @FindBy(xpath = "//div[contains(text(),'Active')]")
    WebElement Active;

    @FindBy(xpath = "//mat-row[@role='row']/mat-cell[11]/div/button/span[1]/mat-icon")
    WebElement ClickAction;

    @FindBy(xpath = "//div[starts-with(@class,'mat-menu-content')]")
    WebElement ActionPopup;

    @FindBy(xpath = "//div[starts-with(@class,'mat-menu-content')]/button[1]/span")
    WebElement BeginClaim;

    @FindBy(xpath = "//div[@class='content-wrapper']/fuse-content/fuse-policyholder-detail/div/div[1]/fuse-policy-tabs/div/button[2]/span[1]/div")
    WebElement GenerateQuoteTab;

    @FindBy(xpath = "//div[contains(text(),'Policy Information')]")
    WebElement ContentPolicyInformation;



    public PolicySearch(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
        baseClass=new BaseClass();
        PageFactory.initElements(driver,this);
    }

    public void PolicySearchTab()
    {
        PolicySearchTab.click();
        waitHelper.waitForElementPresent(driver,PolicySearchTextBox,20);
    }

    public void EnterPolicyNumber() throws IOException, ParseException {
        PolicySearchTextBox.sendKeys(baseClass.ReadJSONData("PolicySearch"));
        waitHelper.waitForElementPresent(driver,Active,60);
    }

    public void GotoAction()
    {
        ClickAction.click();
        waitHelper.waitForElementPresent(driver,ActionPopup,20);
    }

    public void ClickBeginClaim()
    {
        BeginClaim.click();
        waitHelper.waitForElementPresent(driver,GenerateQuoteTab,20);
        waitHelper.waitForElementPresent(driver,ContentPolicyInformation,20);
    }





}
