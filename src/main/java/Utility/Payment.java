package Utility;

import helper.WaitHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Payment {
    WebDriver driver;
    WaitHelper waitHelper;

    @FindBy(xpath = "//div[contains(text(),'APPROVE ')]")
    WebElement ApproveTab;

    @FindBy(xpath = "//h2[contains(text(),'Are you sure you want to approve this claim?')]")
    WebElement ApproveButtonConfirmDiolog;

    @FindBy(xpath = "//span[contains(text(),'CONFIRM')]")
    WebElement ConfirmButton;

    @FindBy(xpath = "//div[@id='caseload']/fuse-action-panel/div/div/div[2]/button[4]/span[1]/mat-icon")
    public WebElement PaymentTab;

    @FindBy(xpath = "//div[contains(text(),'Payment Details')]")
    WebElement ContentPaymentDetail;

    @FindBy(xpath = "//span[contains(text(),'Payment Review')]")
    WebElement PendingReview;

    @FindBy(xpath = "//div[contains(text(),'MARK SETTLED')]")
    WebElement MarkSettledTab;

    @FindBy(xpath = "//textarea[@id='reason']")
    WebElement EnterReason;

    @FindBy(xpath = "//span[contains(text(),' LP Claim')]")
    WebElement LpClaim;

    @FindBy(xpath = "//div[contains(text(),'PAYEES')]")
    WebElement Payee;

    public Payment(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
        PageFactory.initElements(driver,this);
    }

    public void ClickApproveTab()
    {
        ApproveTab.click();
    }

    public void ApproveConfirmDiolog()
    {
        waitHelper.waitForElementPresent(driver,ApproveButtonConfirmDiolog,20);
        ConfirmButton.click();
        waitHelper.waitForElementPresent(driver,PaymentTab,20);
    }

    public void ClickPaymentTab()
    {
        PaymentTab.click();
        waitHelper.waitForElementPresent(driver,ContentPaymentDetail,20);
        waitHelper.waitForElementPresent(driver,ApproveTab,20);
    }

    public void CheckStatusPendingReview()
    {
        String ActualOutput= PendingReview.getText();
        String ExpectedOutput="Payment Review";
        Assert.assertEquals(ActualOutput,ExpectedOutput,"String are Not matched");
    }


}
