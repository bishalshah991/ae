package Utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Locators {

    WebDriver driver;

    public Locators(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    /*
    Locators of Assign FH
     */
    @FindBy(xpath = "//div[contains(text(),'ASSIGN FH')]")
    public WebElement ClickAssignFh;

    @FindBy(xpath = "//span[contains(text(),'Select a Funeral Home')]")
    public WebElement SelectFuneralHomePopup;

    @FindBy(xpath = "//div[@id='forms']/mat-toolbar/mat-toolbar-row/button")
    public WebElement CrossIcon;

    /*
    Locators of Generate Quote
     */

    @FindBy(xpath = "//div[@class='content-wrapper']/fuse-content/fuse-policyholder-detail/div/div[1]/fuse-policy-tabs/div/button[2]/span[1]/div")
    public WebElement GenerateQuote;
}
