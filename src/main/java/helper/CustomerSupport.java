package helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerSupport {
    WebDriver driver;
    WaitHelper waitHelper;

    @FindBy(xpath = "//div[contains(text(),'CUSTOMER SUPPORT')]")
    WebElement CustomerSupport;

    @FindBy(xpath = "//div[@id='caseload']/div[1]/div/button/span[1]/mat-icon")
    WebElement CustomerIcon;

    @FindBy(xpath = "//div[contains(text(),'BACK')]")
    WebElement BackArrowBtn;

    @FindBy(xpath = "//*[@data-placeholder='Type your message']")
    WebElement TypeMessage;

    @FindBy(xpath = "//mat-icon[contains(text(),'send')]")
    WebElement SendButton;


    public CustomerSupport(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public void GotoCustomerSupport() throws InterruptedException {
        CustomerSupport.click();
        Thread.sleep(5000);
    }

    public void TypeYourMessage() throws InterruptedException {
        TypeMessage.sendKeys("This is a test message");
        SendButton.click();
        Thread.sleep(3000);
        BackArrowBtn.click();
        Thread.sleep(5000);
    }
}
