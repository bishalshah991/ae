package helper;

import TestBase.BaseClass;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.util.Arrays;

public class AddBeneficiaries {
    WebDriver driver;
    WaitHelper waitHelper;
    BaseClass baseClass;


    @FindBy(xpath = "//div[contains(text(),'BENEFICIARIES')]")
    public WebElement Beneficiaries;


    @FindBy(xpath = "//div[contains(text(),'Beneficiary on File')]")
    WebElement ContentContactPopup;


    @FindBy(xpath = "//div[@id='forms']/div/button/span[1]")
    WebElement ClickAddIcon;

    @FindBy(xpath = "//div[contains(text(),'Beneficiary Information')]")
    WebElement TextBeneficiaryInformation;

    @FindBy(xpath = "//input[@id='FirstName']")
    WebElement TextFirstname;

    @FindBy(xpath = "//input[@id='LastName']")
    WebElement TextLastname;

    @FindBy(xpath = "//input[@id='AddressLine1']")
    WebElement TextAddress;

    @FindBy(xpath = "//input[@id='City']")
    WebElement TextCity;

    @FindBy(xpath = "//input[@id='State']")
    WebElement TextState;

    @FindBy(xpath = "//input[@id='PostalCode']")
    WebElement TextPostalCode;

    @FindBy(xpath = "//span[contains(text(),'SAVE RECORD ')]")
    WebElement SaveRecordButton;

    @FindBy(xpath = "//div[contains(text(),'//div[contains(text(),'Beneficiary Information')]")
    WebElement TabBeneficiaryFile;

    @FindBy(xpath = "//*[@id='forms']/mat-toolbar/button")
    WebElement CancelIcon;

    public AddBeneficiaries(WebDriver driver) {
        this.driver = driver;
        waitHelper = new WaitHelper(driver);
        baseClass = new BaseClass();
        PageFactory.initElements(driver, this);
    }

    public void AddBeneficiary() {
        Beneficiaries.click();
        waitHelper.waitForElementPresent(driver, ContentContactPopup, 20);
    }

    public void CreateBeneficiary() throws InterruptedException {
        ClickAddIcon.click();
        Thread.sleep(3000);
    }

    public void EnterFnameLname() throws IOException, ParseException {
        TextFirstname.sendKeys(baseClass.ReadJSONData("FirstName1"));
        TextLastname.sendKeys(baseClass.ReadJSONData("LastName1"));
    }

    public void EnterDomesTicAddress() throws InterruptedException, IOException, ParseException {
        TextAddress.sendKeys(baseClass.ReadJSONData("Address"));
        TextCity.sendKeys(baseClass.ReadJSONData("City"));
        TextState.sendKeys(baseClass.ReadJSONData("State"));
        TextPostalCode.sendKeys(baseClass.ReadJSONData("PostalCode"));
        SaveRecordButton.click();
        Thread.sleep(3000);
        CancelIcon.click();
        Thread.sleep(3000);
    }



}


