package helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WaitHelper {
    private static WebDriver driver;

    public WaitHelper(WebDriver _driver)
    {
        this.driver = _driver;
        PageFactory.initElements(driver,this);
    }


    public void waitForElementPresent(WebDriver driver, WebElement element, int timeout)
    {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        wait.until(ExpectedConditions.visibilityOf(element));

        System.out.println("Element found..."+element.getText());
    }

    public WebElement waitForElement(WebDriver driver, int timeout,WebElement element)
    {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }


}
