package helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdditionalInfo {

    WebDriver driver;
    WaitHelper waitHelper;

    @FindBy(xpath = "//div[contains(text(),'ADDITIONAL INFO')]")
    WebElement ClickAdditionalInfo;

    @FindBy(xpath = "//span[contains(text(),'Additional Info')]")
    WebElement ContentAdditionalInfo;

    @FindBy(xpath = "//div[@class='dialog-content-wrapper']/mat-toolbar/button/span[1]")
    WebElement CancelIcon;

    public AdditionalInfo(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
        PageFactory.initElements(driver,this);
    }

    public void ClickAdditionalInfoTab() throws InterruptedException {
        Thread.sleep(5000);
        ClickAdditionalInfo.click();
        waitHelper.waitForElementPresent(driver,ContentAdditionalInfo,20);
        CancelIcon.click();
        waitHelper.waitForElementPresent(driver,ClickAdditionalInfo,20);
    }

}
