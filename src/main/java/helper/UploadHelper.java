package helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UploadHelper {
    String path="/Users/mac/Documents/Backup/Document/Automation/src/main/resources/Picture.png";
    WebDriver driver;
    WaitHelper waitHelper;

    @FindBy(xpath = "//input[@type='file']")
    public WebElement UploadDocument;

    public UploadHelper(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
        waitHelper=new WaitHelper(driver);
    }

    public void Uploaddocument()
    {
        UploadDocument.sendKeys(path);
    }
}
