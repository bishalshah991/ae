package helper;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class TestClass {

    private static final Logger log = LogManager.getLogger(TestClass.class.getName());


    public static void main(String[] args) throws IOException {
        System.out.println("\n.....Hello World.....!!   \n");

        log.info("Information");
        log.error("Error Message");
        log.warn("Warn Message");
        log.fatal("Fatal Message");

        System.out.println("\n Completed");



    }

}
