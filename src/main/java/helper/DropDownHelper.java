package helper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class DropDownHelper {
    private WebDriver _driver;

    public DropDownHelper(WebDriver driver)
    {
        this._driver = driver;
    }


    public void SelectUsingVisibleValue(WebElement element1, String visibleValue)
    {
        Select select = new Select(element1);
        select.selectByVisibleText(visibleValue);
    }

    public void BootStrapDropDown(String xpath, String value)
    {
        List<WebElement> allDropDownValues=_driver.findElements(By.xpath(xpath));
        int dropDownCount= allDropDownValues.size();
        System.out.println("Total items present in the dropdown : "+dropDownCount);

        for(int i=0;i<dropDownCount;i++)
        {
            if(allDropDownValues.get(i).getText().contains(value))
            {
                allDropDownValues.get(i).click();
                break;
            }
        }


    }
}
