package helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ViewCheckList {
    WebDriver driver;
    WaitHelper waitHelper;

    @FindBy(xpath = "//div[contains(text(),'VIEW CHECKLIST')]")
    WebElement ClickViewcheckList;

    @FindBy(xpath = "//span[contains(text(),'Pending Review')]")
    WebElement TextPendingReview;

    @FindBy(xpath = "//div[contains(text(),'Uploaded')]")
    WebElement TextUpload;

    @FindBy(xpath = "//div[contains(text(),'BACK')]")
    WebElement BackArrowBtn;

    public ViewCheckList(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
        PageFactory.initElements(driver,this);
    }

    public void ClickviewChecklist()
    {
        ClickViewcheckList.click();
        waitHelper.waitForElementPresent(driver,TextPendingReview,20);
        waitHelper.waitForElementPresent(driver,TextUpload,20);
    }

    public void ClickBackArrow()
    {
        BackArrowBtn.click();
        waitHelper.waitForElementPresent(driver,ClickViewcheckList,20);

    }
}
